Hàm cộng hai số được thực hiện bằng JavaScript với:
- file MyBigNmuber.js là hàm thực thi các logic cộng hai số với nhau có chú thích trong file
- file UnitTest.test.js là Hàm giữ nhiệm vũ thực hiện các test case từ file MyBigNmuber.js
- Cách chạy : bật terminal với đường dẫn đến thư mục đang chạy . Tiếp theo nhập npm init -y để tải package.json
. Sau đó để chạy được phần test ta cần nhập npm i --save-dev jest và vào package.json để kiểm tra thư viện jest 
đã được tải chưa . Chỉnh dòng số 7 trong package.json thành "test": "jest" . Sau khi sửa xong chỉ việc đánh npm test vào terminal
để chạy các test case