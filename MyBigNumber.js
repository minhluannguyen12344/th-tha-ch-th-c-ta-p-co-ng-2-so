
 class MyBigNumber {
  
  stringSum(num1, num2) {
    let carry = 0;
    let result = '';
    console.log(`Cộng hai số ${num1} và ${num2} `)
    // tính độ dài hai số
    let i = num1.length-1
    let j = num2.length-1
    // chạy hàm cộng
    while(i>=0 || j >=0)
    {
      let sum =0
      // trường hợp 2 chuỗi số còn số để cộng
      if(i >=0 && j >=0){
        sum =parseInt(num1[i])+parseInt(num2[j])+carry
        console.log(` Lấy ${num1[i]} cộng với ${num2[j]} cộng với số nhớ ${carry} \n bằng ${sum}`)
      }
      // trường hợp chuỗi trên còn dư số để cộng
      else if(i < j){
        sum =parseInt(num2[j])+carry
        console.log(` Lấy ${num2[j]} cộng với số nhớ ${carry} \n bằng ${sum}`)
      }
      // trường hợp chuỗi dưới còn hai số để cộng
      else if(i > j){
        sum =parseInt(num1[i])+carry
        console.log(` Lấy ${num1[i]} cộng với số nhớ ${carry} \n bằng ${sum}`)
      }
      // lấy số nhớ ra
      carry = Math.floor(sum / 10);  
      // trả kết quả 
      result = (sum % 10) + result;
      i--
      j--
      
    }

    if (carry > 0) {
      result = carry + result;
      
    }
    console.log('result:',result)
    return result
  }
  
}
module.exports = MyBigNumber
