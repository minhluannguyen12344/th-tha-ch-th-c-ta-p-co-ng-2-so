

const check = require('./MyBigNumber');
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test1 = new check();
  const result1 = test1.stringSum("99999","1234");
  expect(result1).toBe('101233');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test2 = new check();
  const result2 = test2.stringSum("25","6");
  expect(result2).toBe('31');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test3 = new check();
  const result3 = test3.stringSum("1000","25");
  expect(result3).toBe('1025');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test4 = new check();
  const result4 = test4.stringSum("456","7890");
  expect(result4).toBe('8346');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test5 = new check();
  const result5 = test5.stringSum("12","345");
  expect(result5).toBe('357');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test6 = new check();
  const result6 = test6.stringSum("987","6");
  expect(result6).toBe('993');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test7 = new check();
  const result7 = test7.stringSum("1234","567");
  expect(result7).toBe('1801');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test8 = new check();
  const result8 = test8.stringSum("123456","789");
  expect(result8).toBe('124245');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test9 = new check();
  const result9 = test9.stringSum("100","20000");
  expect(result9).toBe('20100');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test10 = new check();
  const result10 = test10.stringSum("1","999999");
  expect(result10).toBe('1000000');
});
test('MyClass.myMethod should return the sum of two numbers', () => {
  const test11 = new check();
  const result11 = test11.stringSum("777","88888");
  expect(result11).toBe('89665');
});